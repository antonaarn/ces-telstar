﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Telnet.Interface;

namespace Telnet.API
{
    /// <summary>
    /// RouteRespDTO is a DTO class to send a Response from external services with a cheapest and fastest route.
    /// </summary>
    public class RouteRespDTO : IRouteRespDTO
    {
        [JsonProperty("accept", Required = Required.Always)]
        [Required]
        public bool Accept { get; set; }

        [JsonProperty("cost", Required = Required.Always)]
        [Required]
        public double Cost { get; set; }

        [JsonProperty("minutes", Required = Required.Always)]
        [Required]
        public double Minutes { get; set; }
    }
}
