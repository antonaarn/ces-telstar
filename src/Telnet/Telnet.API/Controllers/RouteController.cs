﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using System.IO;
using Telnet.Lib;
using Telnet.Interface;

namespace Telnet.API.Controllers
{
    /// <summary>
    /// API Controller. Class used for defining endpoints for Telstar API. It is an entry point to the information from Telstar API.
    /// Base url is api/Route. It produces application/json.
    /// Inheritates from ControllerBase
    /// </summary>
    [ApiController]
    [Route("api/Route")]
    [Produces("application/json")]
    public class RouteController : ControllerBase
    {
        private readonly ILogger<RouteController> _logger;

        public RouteController(ILogger<RouteController> logger)
        {
            _logger = logger;
        }

        /// <summary>
        /// GetRoutes endpoint is an entry point to Telstar API. 
        /// It is a HTTP POST Method.
        /// </summary>
        /// <param name="patchDoc">RouteReqDTO - in order to get information, body with RouteReqDTO should be created.</param>
        /// <returns>It returns ObjectResult which is made from RouteRespDTO</returns>
        [HttpPost]
        public IActionResult GetRoutes([FromBody] RouteReqDTO patchDoc)
        {
            if (patchDoc == null) return BadRequest();

            //patchDoc.Categories;
            // Check request category is in Enum of categories
            if(patchDoc.Categories != null)
            {
                foreach (var cat in patchDoc.Categories)
                {
                    if (!System.Enum.IsDefined(typeof(Constants.Categories), cat)) return BadRequest();
                }
            }
            

            //patchDoc.Date;
            // Check request Date can be parsed as a date
            DateTime dateTime;
            if (!DateTime.TryParseExact(patchDoc.Date, "yyyy-MM-dd", CultureInfo.InvariantCulture,
                DateTimeStyles.None, out dateTime)) return BadRequest();
            if (DateTime.Parse(patchDoc.Date).CompareTo(DateTime.Now) > 0) return BadRequest();

            //patchDoc.Destination;
            if (!System.Enum.IsDefined(typeof(Constants.CityShortcode), patchDoc.Destination)) return BadRequest();

            //patchDoc.Origin;
            if (!System.Enum.IsDefined(typeof(Constants.CityShortcode), patchDoc.Origin)) return BadRequest();

            if (patchDoc.Origin == patchDoc.Destination) return new ObjectResult(new RouteRespDTO() { Accept = false, Cost = 0, Minutes = 0 });

            //patchDoc.Password;
            // This field is optional, hence no validation

            //patchDoc.Size;
            if (patchDoc.Size.Length != 3) return BadRequest();
            if (patchDoc.Size.Any(n => n <= 0)) return BadRequest();

            //patchDoc.Weight;
            if (patchDoc.Weight > 40) return new ObjectResult(new RouteRespDTO() { Accept = false, Cost = 0, Minutes = 0 });

            IRouteRespDTO resp = new RouteRespDTO();
            RouteManager routeManager = new RouteManager();
            routeManager.ConvertExternalSummaryToRouteRespDTO(new ExternalRouteSummary(), ref resp);
            return new ObjectResult(resp);

        }
    }
}
