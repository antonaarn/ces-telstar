﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Telnet.API
{
    /// <summary>
    /// External Route Connector is an entry point to get information to Telstar Logistics.
    /// </summary>
    public static class ExternalRouteConnector
    {

        /// <summary>
        /// SendRouteRequest is an method, which finds and prepare routes. 
        /// </summary>
        /// <param name="apiUrl">url to api, to which should be send reposne.</param>
        /// <returns>Returns Route Resp Dto, which stores calculated best Routes.</returns>
        public static RouteRespDTO SendRouteRequest(string apiUrl)
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(apiUrl);
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";

            var exampleRequest = new RouteReqDTO() { Categories = new string[] { "A", "B", "C" }, Date = DateTime.Now.Date.ToShortDateString(), Destination = "CPH", Origin = "MAL", Size = new double[] { 5, 3, 5 }, Weight = 10, Password = ""};
            
            var json = JsonConvert.SerializeObject(exampleRequest);
            var cleanJsonObject = JObject.Parse(json);

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                streamWriter.Write(cleanJsonObject);
            }

            var result = string.Empty;
            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                result = streamReader.ReadToEnd();
            }
            var unescapedResult = JObject.Parse(result);

            try
            {
                var deserializedDTO = JsonConvert.DeserializeObject<RouteRespDTO>(unescapedResult.ToString());

                if(deserializedDTO != null && deserializedDTO is RouteRespDTO)
                {
                    return deserializedDTO;
                }
                return null;
            }
            catch(Exception e)
            {

            }
            return null;
        }
    }
}
