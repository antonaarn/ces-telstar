﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Telnet.Interface;

namespace Telnet.API
{
    /// <summary>
    /// RouteReqDTO is a DTO class to send a request for a cheapest and fastest route from external services.
    /// </summary>
    public class RouteReqDTO : IRouteReqDTO
    {
        [JsonProperty("origin", Required = Required.Always)]
        public string Origin { get; set; }

        [JsonProperty("destination", Required = Required.Always)]
        public string Destination { get; set; }

        [JsonProperty("weight", Required = Required.Always)]
        public double Weight { get; set; }

        [JsonProperty("size", Required = Required.Always)]
        public double[] Size { get; set; }

        [JsonProperty("categories", Required = Required.Always)]
        public string[] Categories { get; set; }


        [JsonIgnore]
        private DateTime _date;

        [JsonProperty("date", Required = Required.Always)]
        [Required]
        [DataType(DataType.Date)]
        public string Date
        {
            get
            {
                return _date.ToString("yyyy/MM/dd");
            }
            set
            {
                DateTime.TryParseExact(value, "yyyy/MM/dd", CultureInfo.CurrentCulture, DateTimeStyles.None, out _date);
            }
        }

        [JsonProperty("password", Required = Required.AllowNull)]
        public string Password { get; set; }

    }
}
