﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Telnet.Web.Data
{
    /// <summary>
    /// Entity to store Route in the Database. It is an entity class
    /// </summary>
    public class Route
    {
        public long ID { get; set; }
        
        public long OriginID { get; set; }
        
        public long DestinationID { get; set; }

        public double Price { get; set; }

        public int Time { get; set; }

        public string Operators { get; set; }

        public DateTime CreateTime { get; set; }

        public virtual City Origin { get; set; }

        public virtual City Destination { get; set; }

    }
}
