﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;


namespace Telnet.Web.Data
{
    /// <summary>
    /// Application Db Context is used for creation connection to database. 
    /// Inherites from IdentityDbContext for a Security reason. Instead of DBContext
    /// </summary>
    public class ApplicationDbContext : IdentityDbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        public DbSet<City> Cities { get; set; }
        public DbSet<CitySegments> CitySegments { get; set; }
        public DbSet<Route> Routes { get; set; }

        /// <summary>
        /// OnModelCreating function is used for migration reason. It creates also constrains and other requirements due to the cascade deletion.
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<City>()
                .Property(b => b.ID)
                .IsRequired();

            modelBuilder.Entity<CitySegments>()
                .Property(b => b.ID)
                .IsRequired();

            modelBuilder.Entity<CitySegments>()
                .HasOne(b => b.FirstCity)
                .WithMany()
                .HasForeignKey(b => b.FirstCityID)
                .OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<CitySegments>()
              .HasOne(b => b.Neighbor)
              .WithMany()
              .HasForeignKey(b => b.NeighborID)
              .OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<Route>()
                .Property(b => b.ID)
                .IsRequired();

            modelBuilder.Entity<Route>()
              .HasOne(b => b.Origin)
              .WithMany()
              .HasForeignKey(b => b.OriginID)
              .OnDelete(DeleteBehavior.NoAction);


            modelBuilder.Entity<Route>()
              .HasOne(b => b.Destination)
              .WithMany()
              .HasForeignKey(b => b.DestinationID)
              .OnDelete(DeleteBehavior.NoAction);
        }
    }
}





