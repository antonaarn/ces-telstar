﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Telnet.Web.Data.Migrations
{
    public partial class NumberOfSegments : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "NumberOfSegments",
                table: "CitySegments",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "NumberOfSegments",
                table: "CitySegments");
        }
    }
}
