SET IDENTITY_INSERT CITIES ON


INSERT INTO CITIES(Name,Code, PartOfContinent, AccessByCar, AccessByPlane, AccessByShip, CreateTime, ID) VALUES ('Addis Abeba','ADD','North-East', 1, 0, 0, CURRENT_TIMESTAMP, 1);
INSERT INTO CITIES(Name,Code, PartOfContinent, AccessByCar, AccessByPlane, AccessByShip, CreateTime, ID) VALUES ('Amatave','AMA', 'South-East',  0, 1, 1, CURRENT_TIMESTAMP, 2);
INSERT INTO CITIES(Name,Code, PartOfContinent, AccessByCar, AccessByPlane, AccessByShip, CreateTime, ID) VALUES ('Bahr El Ghazal','BAH', 'North-East',  1, 0, 0, CURRENT_TIMESTAMP, 3);
INSERT INTO CITIES(Name,Code, PartOfContinent, AccessByCar, AccessByPlane, AccessByShip, CreateTime, ID) VALUES ('Cairo','CAI', 'North-East',  1, 1, 1, CURRENT_TIMESTAMP, 4);
INSERT INTO CITIES(Name,Code, PartOfContinent, AccessByCar, AccessByPlane, AccessByShip, CreateTime, ID) VALUES ('Congo','CON', 'South-West',  1, 0, 0, CURRENT_TIMESTAMP, 5);
INSERT INTO CITIES(Name,Code, PartOfContinent, AccessByCar, AccessByPlane, AccessByShip, CreateTime, ID) VALUES ('Dakar','DAK', 'North-East',  1, 0, 1, CURRENT_TIMESTAMP, 6);
INSERT INTO CITIES(Name,Code, PartOfContinent, AccessByCar, AccessByPlane, AccessByShip, CreateTime, ID) VALUES ('Darfur','DAR', 'North-East',  1, 1, 0, CURRENT_TIMESTAMP, 7);
INSERT INTO CITIES(Name,Code, PartOfContinent, AccessByCar, AccessByPlane, AccessByShip, CreateTime, ID) VALUES ('De Kanariske �er','DEK', 'North-West',  0, 0, 1, CURRENT_TIMESTAMP, 8);
INSERT INTO CITIES(Name,Code, PartOfContinent, AccessByCar, AccessByPlane, AccessByShip, CreateTime, ID) VALUES ('Dragebjerget','DRA', 'South-East',  1, 1, 0, CURRENT_TIMESTAMP, 9);
INSERT INTO CITIES(Name,Code, PartOfContinent, AccessByCar, AccessByPlane, AccessByShip, CreateTime, ID) VALUES ('Guld-kysten','GUL', 'North-West',  1, 1, 1, CURRENT_TIMESTAMP, 10);
INSERT INTO CITIES(Name,Code, PartOfContinent, AccessByCar, AccessByPlane, AccessByShip, CreateTime, ID) VALUES ('Hvalbugten','HVA', 'South-West',  1, 1, 1, CURRENT_TIMESTAMP, 11);
INSERT INTO CITIES(Name,Code, PartOfContinent, AccessByCar, AccessByPlane, AccessByShip, CreateTime, ID) VALUES ('Kabalo','KAB', 'South-East',  1, 1, 0, CURRENT_TIMESTAMP, 12);
INSERT INTO CITIES(Name,Code, PartOfContinent, AccessByCar, AccessByPlane, AccessByShip, CreateTime, ID) VALUES ('Kap Guardafui','KAG', 'North-West',  1, 1, 1, CURRENT_TIMESTAMP, 13);
INSERT INTO CITIES(Name,Code, PartOfContinent, AccessByCar, AccessByPlane, AccessByShip, CreateTime, ID) VALUES ('Kap St. Marie','KAM', 'South-West',  0, 1, 1, CURRENT_TIMESTAMP, 14);
INSERT INTO CITIES(Name,Code, PartOfContinent, AccessByCar, AccessByPlane, AccessByShip, CreateTime, ID) VALUES ('Kapstaden','KAS', 'South-West',  1, 1, 1, CURRENT_TIMESTAMP, 15);
INSERT INTO CITIES(Name,Code, PartOfContinent, AccessByCar, AccessByPlane, AccessByShip, CreateTime, ID) VALUES ('Luanda','LUA', 'South-East',  1, 1, 1, CURRENT_TIMESTAMP, 16);
INSERT INTO CITIES(Name,Code, PartOfContinent, AccessByCar, AccessByPlane, AccessByShip, CreateTime, ID) VALUES ('Marrakesh','MAR', 'North-East',  1, 1, 0, CURRENT_TIMESTAMP, 17);
INSERT INTO CITIES(Name,Code, PartOfContinent, AccessByCar, AccessByPlane, AccessByShip, CreateTime, ID) VALUES ('Mocambique','MOC', 'South-West',  1, 0, 1, CURRENT_TIMESTAMP, 18);
INSERT INTO CITIES(Name,Code, PartOfContinent, AccessByCar, AccessByPlane, AccessByShip, CreateTime, ID) VALUES ('Omdurman','OMD', 'North-East',  1, 0, 0, CURRENT_TIMESTAMP, 19);
INSERT INTO CITIES(Name,Code, PartOfContinent, AccessByCar, AccessByPlane, AccessByShip, CreateTime, ID) VALUES ('Sahara','SAH', 'North-West',  1, 0, 0, CURRENT_TIMESTAMP, 20);
INSERT INTO CITIES(Name,Code, PartOfContinent, AccessByCar, AccessByPlane, AccessByShip, CreateTime, ID) VALUES ('Sierra Leone','SIE', 'North-West',  1, 1, 1, CURRENT_TIMESTAMP, 21);
INSERT INTO CITIES(Name,Code, PartOfContinent, AccessByCar, AccessByPlane, AccessByShip, CreateTime, ID) VALUES ('Slave-kysten','SLA', 'North-West',  1, 0, 1, CURRENT_TIMESTAMP, 22);
INSERT INTO CITIES(Name,Code, PartOfContinent, AccessByCar, AccessByPlane, AccessByShip, CreateTime, ID) VALUES ('St. Helena','STH', 'South-West',  0, 1, 1, CURRENT_TIMESTAMP, 23);
INSERT INTO CITIES(Name,Code, PartOfContinent, AccessByCar, AccessByPlane, AccessByShip, CreateTime, ID) VALUES ('Suakin','SUA', 'North-West',  1, 1, 0, CURRENT_TIMESTAMP, 24);
INSERT INTO CITIES(Name,Code, PartOfContinent, AccessByCar, AccessByPlane, AccessByShip, CreateTime, ID) VALUES ('Tanger','TAN', 'North-East',  1, 1, 1, CURRENT_TIMESTAMP, 25);
INSERT INTO CITIES(Name,Code, PartOfContinent, AccessByCar, AccessByPlane, AccessByShip, CreateTime, ID) VALUES ('Timbuktu','TIM', 'North-East',  1, 0, 0, CURRENT_TIMESTAMP, 26);
INSERT INTO CITIES(Name,Code, PartOfContinent, AccessByCar, AccessByPlane, AccessByShip, CreateTime, ID) VALUES ('Tripoli','TRI', 'North-East',  1, 1, 0, CURRENT_TIMESTAMP, 27);
INSERT INTO CITIES(Name,Code, PartOfContinent, AccessByCar, AccessByPlane, AccessByShip, CreateTime, ID) VALUES ('Tunis','TUN', 'North-East',  1, 0, 1, CURRENT_TIMESTAMP, 28);
INSERT INTO CITIES(Name,Code, PartOfContinent, AccessByCar, AccessByPlane, AccessByShip, CreateTime, ID) VALUES ('Victoria-faldene','VIF', 'South-East',  1, 0, 0, CURRENT_TIMESTAMP, 29);
INSERT INTO CITIES(Name,Code, PartOfContinent, AccessByCar, AccessByPlane, AccessByShip, CreateTime, ID) VALUES ('Victoria-s�en','VIS', 'North-East',  1, 1, 0, CURRENT_TIMESTAMP, 30);
INSERT INTO CITIES(Name,Code, PartOfContinent, AccessByCar, AccessByPlane, AccessByShip, CreateTime, ID) VALUES ('Wadai','WAD', 'North-East',  1, 0, 0, CURRENT_TIMESTAMP, 31);
INSERT INTO CITIES(Name,Code, PartOfContinent, AccessByCar, AccessByPlane, AccessByShip, CreateTime, ID) VALUES ('Zanzibar','ZAN', 'South-East',  1, 0, 0, CURRENT_TIMESTAMP, 32);

SET IDENTITY_INSERT CITIES OFF
SET IDENTITY_INSERT CITYSEGMENTS ON

INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments, CreateTime, Operator, ID) VALUES (17, 25, 2, CURRENT_TIMESTAMP, 0, 1);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (28, 25, 5, CURRENT_TIMESTAMP, 0, 2);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (20, 25, 5, CURRENT_TIMESTAMP, 0, 3);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (25, 17, 2, CURRENT_TIMESTAMP, 0, 4);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (20, 17, 5, CURRENT_TIMESTAMP, 0, 5);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (6, 17, 8, CURRENT_TIMESTAMP, 0, 6);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (25, 28, 5, CURRENT_TIMESTAMP, 0, 7);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (27, 28, 3, CURRENT_TIMESTAMP, 0, 8);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (25, 20, 5, CURRENT_TIMESTAMP, 0, 9);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (17, 20, 5, CURRENT_TIMESTAMP, 0, 10);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (7, 20, 8, CURRENT_TIMESTAMP, 0, 11);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (28, 27, 3, CURRENT_TIMESTAMP, 0, 12);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (19, 27, 6, CURRENT_TIMESTAMP, 0, 13);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (7, 31, 4, CURRENT_TIMESTAMP, 0, 14);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (22, 31, 7, CURRENT_TIMESTAMP, 0, 15);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (5, 31, 6, CURRENT_TIMESTAMP, 0, 16);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (27, 19, 6, CURRENT_TIMESTAMP, 0, 17);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (4, 19, 4, CURRENT_TIMESTAMP, 0, 18);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (7, 19, 3, CURRENT_TIMESTAMP, 0, 19);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (19, 4, 4, CURRENT_TIMESTAMP, 0, 20);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (20, 7, 8, CURRENT_TIMESTAMP, 0, 21);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (31, 7, 4, CURRENT_TIMESTAMP, 0, 22);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (19, 7, 3, CURRENT_TIMESTAMP, 0, 23);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (24, 7, 4, CURRENT_TIMESTAMP, 0, 24);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (22, 7, 7, CURRENT_TIMESTAMP, 0, 25);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (5, 7, 6, CURRENT_TIMESTAMP, 0, 26);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (3, 7, 2, CURRENT_TIMESTAMP, 0, 27);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (7, 24, 4, CURRENT_TIMESTAMP, 0, 28);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (1, 24, 3, CURRENT_TIMESTAMP, 0, 29);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (24, 1, 3, CURRENT_TIMESTAMP, 0, 30);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (13, 1, 3, CURRENT_TIMESTAMP, 0, 31);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (30, 1, 3, CURRENT_TIMESTAMP, 0, 32);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (1, 13, 3, CURRENT_TIMESTAMP, 0, 33);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (32, 13, 5, CURRENT_TIMESTAMP, 0, 34);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (17, 6, 8, CURRENT_TIMESTAMP, 0, 35);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (21, 6, 4, CURRENT_TIMESTAMP, 0, 36);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (21, 26, 5, CURRENT_TIMESTAMP, 0, 37);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (10, 26, 4, CURRENT_TIMESTAMP, 0, 38);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (22, 26, 5, CURRENT_TIMESTAMP, 0, 39);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (6, 21, 4, CURRENT_TIMESTAMP, 0, 40);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (26, 21, 5, CURRENT_TIMESTAMP, 0, 41);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (10, 21, 5, CURRENT_TIMESTAMP, 0, 42);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (26, 10, 4, CURRENT_TIMESTAMP, 0, 43);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (21, 10, 5, CURRENT_TIMESTAMP, 0, 44);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (31, 22, 7, CURRENT_TIMESTAMP, 0, 45);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (7, 22, 7, CURRENT_TIMESTAMP, 0, 46);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (26, 22, 5, CURRENT_TIMESTAMP, 0, 47);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (5, 22, 5, CURRENT_TIMESTAMP, 0, 48);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (31, 5, 6, CURRENT_TIMESTAMP, 0, 49);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (7, 5, 6, CURRENT_TIMESTAMP, 0, 50);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (22, 5, 5, CURRENT_TIMESTAMP, 0, 51);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (16, 5, 3, CURRENT_TIMESTAMP, 0, 52);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (30, 12, 4, CURRENT_TIMESTAMP, 0, 53);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (16, 12, 4, CURRENT_TIMESTAMP, 0, 54);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (7, 3, 2, CURRENT_TIMESTAMP, 0, 55);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (30, 3, 2, CURRENT_TIMESTAMP, 0, 56);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (1, 30, 3, CURRENT_TIMESTAMP, 0, 57);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (12, 30, 4, CURRENT_TIMESTAMP, 0, 58);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (3, 30, 2, CURRENT_TIMESTAMP, 0, 59);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (18, 30, 6, CURRENT_TIMESTAMP, 0, 60);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (13, 32, 6, CURRENT_TIMESTAMP, 0, 61);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (18, 32, 3, CURRENT_TIMESTAMP, 0, 62);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (30, 18, 6, CURRENT_TIMESTAMP, 0, 63);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (32, 18, 3, CURRENT_TIMESTAMP, 0, 64);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (16, 18, 10, CURRENT_TIMESTAMP, 0, 65);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (29, 18, 5, CURRENT_TIMESTAMP, 0, 66);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (9, 18, 4, CURRENT_TIMESTAMP, 0, 67);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (5, 16, 3, CURRENT_TIMESTAMP, 0, 68);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (12, 16, 4, CURRENT_TIMESTAMP, 0, 69);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (18, 16, 10, CURRENT_TIMESTAMP, 0, 70);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (29, 16, 11, CURRENT_TIMESTAMP, 0, 71);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (9, 16, 11, CURRENT_TIMESTAMP, 0, 72);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (18, 29, 5, CURRENT_TIMESTAMP, 0, 73);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (16, 29, 11, CURRENT_TIMESTAMP, 0, 74);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (9, 29, 3, CURRENT_TIMESTAMP, 0, 75);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (11, 29, 4, CURRENT_TIMESTAMP, 0, 76);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (18, 9, 4, CURRENT_TIMESTAMP, 0, 77);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (16, 9, 11, CURRENT_TIMESTAMP, 0, 78);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (29, 9, 3, CURRENT_TIMESTAMP, 0, 79);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (11, 15, 4, CURRENT_TIMESTAMP, 0, 80);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (29, 11, 4, CURRENT_TIMESTAMP, 0, 81);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (5, 11, 4, CURRENT_TIMESTAMP, 0, 82);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (25, 8, 99, CURRENT_TIMESTAMP, 2, 83);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (8, 25, 99, CURRENT_TIMESTAMP, 2, 84);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (8, 6, 99, CURRENT_TIMESTAMP, 2, 85);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (6, 8, 99, CURRENT_TIMESTAMP, 2, 86);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (6, 21, 99, CURRENT_TIMESTAMP, 2, 87);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (21, 6, 99, CURRENT_TIMESTAMP, 2, 88);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (21, 10, 99, CURRENT_TIMESTAMP, 2, 89);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (10, 21, 99, CURRENT_TIMESTAMP, 2, 90);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (10, 22, 99, CURRENT_TIMESTAMP, 2, 91);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (22, 10, 99, CURRENT_TIMESTAMP, 2, 92);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (22, 11, 99, CURRENT_TIMESTAMP, 2, 93);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (22, 11, 99, CURRENT_TIMESTAMP, 2, 94);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (15, 11, 99, CURRENT_TIMESTAMP, 2, 95);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (11, 15, 99, CURRENT_TIMESTAMP, 2, 96);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (21, 23, 99, CURRENT_TIMESTAMP, 2, 97);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (26, 21, 99, CURRENT_TIMESTAMP, 2, 98);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (6, 23, 99, CURRENT_TIMESTAMP, 2, 99);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (23, 6, 99, CURRENT_TIMESTAMP, 2, 100);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (23, 15, 99, CURRENT_TIMESTAMP, 2, 101);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (15, 23, 99, CURRENT_TIMESTAMP, 2, 102);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (15, 14, 99, CURRENT_TIMESTAMP, 2, 103);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (14, 15, 99, CURRENT_TIMESTAMP, 2, 104);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (14, 18, 99, CURRENT_TIMESTAMP, 2, 105);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (18, 14, 99, CURRENT_TIMESTAMP, 2, 106);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (18, 13, 99, CURRENT_TIMESTAMP, 2, 107);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (13, 18, 99, CURRENT_TIMESTAMP, 2, 108);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (2, 13, 99, CURRENT_TIMESTAMP, 2, 109);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (13, 2, 99, CURRENT_TIMESTAMP, 2, 110);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (24, 13, 99, CURRENT_TIMESTAMP, 2, 111);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (13, 24, 99, CURRENT_TIMESTAMP, 2, 112);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (24, 4, 99, CURRENT_TIMESTAMP, 2, 113);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (4, 24, 99, CURRENT_TIMESTAMP, 2, 114);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (28, 4, 99, CURRENT_TIMESTAMP, 2, 115);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (4, 28, 99, CURRENT_TIMESTAMP, 2, 116);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (25, 28, 99, CURRENT_TIMESTAMP, 2, 117);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (28, 25, 99, CURRENT_TIMESTAMP, 2, 118);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (11, 23, 99, CURRENT_TIMESTAMP, 2, 119);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments,CreateTime, Operator, ID) VALUES (23, 11, 99, CURRENT_TIMESTAMP, 2, 120);
-- AIRLINES
--TANGER
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments, CreateTime, Operator, ID) VALUES (25, 17, 99, CURRENT_TIMESTAMP, 1, 120);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments, CreateTime, Operator, ID) VALUES (25, 27, 99, CURRENT_TIMESTAMP, 1, 121);

--MARAKESH
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments, CreateTime, Operator, ID) VALUES (17, 25, 99, CURRENT_TIMESTAMP, 1, 122);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments, CreateTime, Operator, ID) VALUES (17, 21, 99, CURRENT_TIMESTAMP, 1, 123);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments, CreateTime, Operator, ID) VALUES (17, 10, 99, CURRENT_TIMESTAMP, 1, 124);

--Tripoli
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments, CreateTime, Operator, ID) VALUES (27, 25, 99, CURRENT_TIMESTAMP, 1, 125);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments, CreateTime, Operator, ID) VALUES (27, 10, 99, CURRENT_TIMESTAMP, 1, 126);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments, CreateTime, Operator, ID) VALUES (27, 7, 99, CURRENT_TIMESTAMP, 1, 127);

-- SIERRA LEONE
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments, CreateTime, Operator, ID) VALUES (21, 17, 99, CURRENT_TIMESTAMP, 1, 128);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments, CreateTime, Operator, ID) VALUES (21, 23, 99, CURRENT_TIMESTAMP, 1, 129);

-- Guld kesten
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments, CreateTime, Operator, ID) VALUES (10, 17, 99, CURRENT_TIMESTAMP, 1, 130);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments, CreateTime, Operator, ID) VALUES (10, 27, 99, CURRENT_TIMESTAMP, 1, 131);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments, CreateTime, Operator, ID) VALUES (10, 16, 99, CURRENT_TIMESTAMP, 1, 132);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments, CreateTime, Operator, ID) VALUES (10, 11, 99, CURRENT_TIMESTAMP, 1, 133);

--DARFUR
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments, CreateTime, Operator, ID) VALUES (7, 27, 99, CURRENT_TIMESTAMP, 1, 134);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments, CreateTime, Operator, ID) VALUES (7, 24, 99, CURRENT_TIMESTAMP, 1, 135);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments, CreateTime, Operator, ID) VALUES (7, 12, 99, CURRENT_TIMESTAMP, 1, 136);

--SUAKIN
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments, CreateTime, Operator, ID) VALUES (24, 7, 99, CURRENT_TIMESTAMP, 1, 137);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments, CreateTime, Operator, ID) VALUES (24, 30, 99, CURRENT_TIMESTAMP, 1, 138);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments, CreateTime, Operator, ID) VALUES (24, 4, 99, CURRENT_TIMESTAMP, 1, 139);

-- CAIRO
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments, CreateTime, Operator, ID) VALUES (4, 24, 99, CURRENT_TIMESTAMP, 1, 140);

-- ST. HELEN
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments, CreateTime, Operator, ID) VALUES (23, 21, 99, CURRENT_TIMESTAMP, 1, 141);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments, CreateTime, Operator, ID) VALUES (23, 15, 99, CURRENT_TIMESTAMP, 1, 142);

-- LUANDA
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments, CreateTime, Operator, ID) VALUES (16, 10, 99, CURRENT_TIMESTAMP, 1, 143);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments, CreateTime, Operator, ID) VALUES (16, 11, 99, CURRENT_TIMESTAMP, 1, 144);

-- KABALO
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments, CreateTime, Operator, ID) VALUES (12, 7, 99, CURRENT_TIMESTAMP, 1, 145);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments, CreateTime, Operator, ID) VALUES (12, 15, 99, CURRENT_TIMESTAMP, 1, 146);

-- Victoria sore
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments, CreateTime, Operator, ID) VALUES (30, 24, 99, CURRENT_TIMESTAMP, 1, 147);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments, CreateTime, Operator, ID) VALUES (30, 13, 99, CURRENT_TIMESTAMP, 1, 148);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments, CreateTime, Operator, ID) VALUES (30, 9, 99, CURRENT_TIMESTAMP, 1, 149);

-- Hvalbugten
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments, CreateTime, Operator, ID) VALUES (11, 10, 99, CURRENT_TIMESTAMP, 1, 150);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments, CreateTime, Operator, ID) VALUES (11, 16, 99, CURRENT_TIMESTAMP, 1, 151);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments, CreateTime, Operator, ID) VALUES (11, 15, 99, CURRENT_TIMESTAMP, 1, 152);

-- Kap gua
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments, CreateTime, Operator, ID) VALUES (13, 30, 99, CURRENT_TIMESTAMP, 1, 153);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments, CreateTime, Operator, ID) VALUES (13, 2, 99, CURRENT_TIMESTAMP, 1, 154);

-- Amatave
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments, CreateTime, Operator, ID) VALUES (2, 13, 99, CURRENT_TIMESTAMP, 1, 155);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments, CreateTime, Operator, ID) VALUES (2, 15, 99, CURRENT_TIMESTAMP, 1, 156);

-- kap st marie
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments, CreateTime, Operator, ID) VALUES (14, 13, 99, CURRENT_TIMESTAMP, 1, 157);

-- kapstaden
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments, CreateTime, Operator, ID) VALUES (13, 14, 99, CURRENT_TIMESTAMP, 1, 158);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments, CreateTime, Operator, ID) VALUES (13, 2, 99, CURRENT_TIMESTAMP, 1, 159);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments, CreateTime, Operator, ID) VALUES (13, 9, 99, CURRENT_TIMESTAMP, 1, 160);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments, CreateTime, Operator, ID) VALUES (13, 12, 99, CURRENT_TIMESTAMP, 1, 161);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments, CreateTime, Operator, ID) VALUES (13, 11, 99, CURRENT_TIMESTAMP, 1, 162);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments, CreateTime, Operator, ID) VALUES (13, 23, 99, CURRENT_TIMESTAMP, 1, 163);

-- Dragebjerget
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments, CreateTime, Operator, ID) VALUES (12, 13, 99, CURRENT_TIMESTAMP, 1, 164);
INSERT INTO CITYSEGMENTS(FirstCityID,NeighborID,NumberOfSegments, CreateTime, Operator, ID) VALUES (12, 30, 99, CURRENT_TIMESTAMP, 1, 165);

SET IDENTITY_INSERT CITYSEGMENTS OFF
