﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.IO;

namespace Telnet.Web.Data.Migrations
{
    public partial class DataInitializer : Migration
    {
        private string Migration => $@"{AppDomain.CurrentDomain.BaseDirectory}..\..\..\Data\Migrations\{(Attribute.GetCustomAttribute(GetType(), typeof(MigrationAttribute)) as MigrationAttribute).Id}";

        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(File.ReadAllText($"{Migration}.sql"));
        }



        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
