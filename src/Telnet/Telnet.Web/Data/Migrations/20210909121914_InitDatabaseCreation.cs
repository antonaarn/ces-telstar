﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Telnet.Web.Data.Migrations
{
    public partial class InitDatabaseCreation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Cities",
                columns: table => new
                {
                    ID = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Code = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PartOfContinent = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    AccessByCar = table.Column<bool>(type: "bit", nullable: false),
                    AccessByPlane = table.Column<bool>(type: "bit", nullable: false),
                    AccessByShip = table.Column<bool>(type: "bit", nullable: false),
                    CreateTime = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Cities", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "CitySegments",
                columns: table => new
                {
                    ID = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FirstCityID = table.Column<long>(type: "bigint", nullable: false),
                    NeighborID = table.Column<long>(type: "bigint", nullable: false),
                    CreateTime = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CitySegments", x => x.ID);
                    table.ForeignKey(
                        name: "FK_CitySegments_Cities_FirstCityID",
                        column: x => x.FirstCityID,
                        principalTable: "Cities",
                        principalColumn: "ID");
                    table.ForeignKey(
                        name: "FK_CitySegments_Cities_NeighborID",
                        column: x => x.NeighborID,
                        principalTable: "Cities",
                        principalColumn: "ID");
                });

            migrationBuilder.CreateTable(
                name: "Routes",
                columns: table => new
                {
                    ID = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    OriginID = table.Column<long>(type: "bigint", nullable: false),
                    DestinationID = table.Column<long>(type: "bigint", nullable: false),
                    Price = table.Column<double>(type: "float", nullable: false),
                    Time = table.Column<int>(type: "int", nullable: false),
                    Operators = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreateTime = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Routes", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Routes_Cities_DestinationID",
                        column: x => x.DestinationID,
                        principalTable: "Cities",
                        principalColumn: "ID");
                    table.ForeignKey(
                        name: "FK_Routes_Cities_OriginID",
                        column: x => x.OriginID,
                        principalTable: "Cities",
                        principalColumn: "ID");
                });

            migrationBuilder.CreateIndex(
                name: "IX_CitySegments_FirstCityID",
                table: "CitySegments",
                column: "FirstCityID");

            migrationBuilder.CreateIndex(
                name: "IX_CitySegments_NeighborID",
                table: "CitySegments",
                column: "NeighborID");

            migrationBuilder.CreateIndex(
                name: "IX_Routes_DestinationID",
                table: "Routes",
                column: "DestinationID");

            migrationBuilder.CreateIndex(
                name: "IX_Routes_OriginID",
                table: "Routes",
                column: "OriginID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CitySegments");

            migrationBuilder.DropTable(
                name: "Routes");

            migrationBuilder.DropTable(
                name: "Cities");
        }
    }
}
