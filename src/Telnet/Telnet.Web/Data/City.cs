﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Telnet.Web.Data
{
    /// <summary>
    /// class made for storing data in database. City class is an entity class.
    /// </summary>
    public class City
    {
        public long ID { get; set; }
        
        public string Name { get; set; }
        
        public string Code { get; set; }
   
        public string PartOfContinent { get; set; }

        public Boolean AccessByCar { get; set; }
   
        public Boolean AccessByPlane { get; set; }
        
        public Boolean AccessByShip { get; set; }

        public DateTime CreateTime { get; set; }

        /// <summary>
        /// Dislay Name is used for better displaying better city in a dropdown menu
        /// </summary>
        public virtual string DisplayName
        {
            get
            {
                return this.Name + " (" + this.PartOfContinent + ")";
            }
        }
    }
}
