﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Telnet.Web.Data
{
    /// <summary>
    /// Entity to store CitySegments in the Database. It is an entity class
    /// </summary>
    public class CitySegments
    {
        public long ID { get; set; }

        public long FirstCityID { get; set; }

        public long NeighborID { get; set; }

        public int NumberOfSegments { get; set; }

        public DateTime CreateTime { get; set; }

        public int Operator { get; set; }

        public virtual City FirstCity { get; set; }

        public virtual City Neighbor { get; set; }
    }
}
