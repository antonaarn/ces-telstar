﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Telnet.Interface;

namespace Telnet.Web.Models
{
    /// <summary>
    /// SearchModel is on the fly class used to store search after form posted
    /// </summary>
    public class SearchModel : ISearchModel
    {
        [Required]
        public string Origin { get; set; }
        [Required]
        public string Destination { get; set; }
        [Required]
        [Range(0, 40, ErrorMessage = "Sorry, we do not accept parcels over 40kg")]
        public double Weight { get; set; }
        [Required]
        public double Height { get; set; }
        [Required]
        public double Width { get; set; }
        [Required]
        public double Depth { get; set; }
        public bool Recommended { get; set; }
        public bool Cautious { get; set; }
        public bool Livestock { get; set; }
        public bool Refrigerated { get; set; }
    }
}
