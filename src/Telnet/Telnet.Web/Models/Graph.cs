﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

/// <summary>
/// Main graph class to create nodes and vertices and compute distances between these
/// </summary>
class Graph
{
    class NodeData
    {
        private int index;
        public string data;
        public NodeData(string data, int index)
        {
            this.index = index;
            this.data = data;
        }
    }
    private List<NodeData> vertices;
    private int graphSize;
    private List<int> allCityIds;
    private StreamReader sr;
    private int[,] adjMatrix;
    private const int infinity = 10000000;
    /// <summary>
    /// Main graph constructor
    /// </summary>
    /// <param name="graphSize"></param>
    /// <param name="Cities"></param>
    /// <param name="travelcosts"></param>
    public Graph(int graphSize, List<(string cityName, int cityId)> Cities, List<(int cityFromIndex, int cityToIndex, int cost)> travelcosts)
    {
        this.graphSize = graphSize;
        vertices = new List<NodeData>();
        allCityIds = new List<int>();
        adjMatrix = new int[graphSize, graphSize];
        for (int i = 0; i < graphSize; i++)
        {
            for (int j = 0; j < graphSize; j++)
            {
                adjMatrix[i, j] = infinity;
            }
        }
        foreach (var city in Cities)
        {
            allCityIds.Add(city.cityId);
        }
        vertices.Add(new NodeData("    ", -1));
        for (int i = 1; i < graphSize; i++)
        {
            var line = Cities[i];
            vertices.Add(new NodeData(line.cityName, line.cityId));
        }
        foreach (var item in travelcosts)
        {
            AddEdge(item.cityFromIndex, item.cityToIndex, item.cost);
        }
    }
    /// <summary>
    /// Run Djikstras algorithm on the nodes and vertices created.
    /// </summary>
    /// <param name="source"></param>
    /// <param name="popLast"></param>
    /// <returns></returns>
    public Dictionary<ValueTuple<int, int>, TravelPath> RunDijkstra(int source, bool popLast = false)
    {
        int[] distance = new int[graphSize];
        int[] previous = new int[graphSize];
        Dictionary<ValueTuple<int, int>, TravelPath> travelPaths = new Dictionary<(int, int), TravelPath>();
        for (int i = 1; i < graphSize; i++)
        {
            distance[i] = infinity;
            previous[i] = infinity;
        }
        distance[source] = 0;
        PriorityQueue<int> pq = new PriorityQueue<int>();
        pq.Enqueue(source, adjMatrix[source, source]);
        for (int i = 1; i < graphSize; i++)
        {
            for (int j = 1; j < graphSize; j++)
            {
                if (adjMatrix[i, j] != adjMatrix[source, source])
                {
                    pq.Enqueue(i, adjMatrix[i, j]);
                }
            }
        }
        while (!pq.Empty())
        {
            int u = pq.dequeue_min();

            for (int v = 1; v < graphSize; v++)
            {
                if (adjMatrix[u, v] < infinity)
                {
                    int alt = distance[u] + adjMatrix[u, v];
                    if (alt < distance[v])
                    {
                        distance[v] = alt;
                        previous[v] = u;
                        pq.Enqueue(u, distance[v]);
                    }
            }
            }
        }
        for (int i = 1; i < graphSize; i++)
        {
            printPath(previous, source, i);
            List<int> thisPath = getPath(previous, source, i, popLast);
            travelPaths[ValueTuple.Create(source, i)] = new TravelPath(thisPath, distance[i]);
        }
        return travelPaths;
    }

    public List<TravelPath> computeMultiplePaths(List<int> endPointCities)
    {
        List<TravelPath> allTravelpaths = new List<TravelPath>();
        Dictionary<ValueTuple<int, int>, TravelPath> originTravelPaths = RunDijkstra(endPointCities[0]);
        Dictionary<ValueTuple<int, int>, TravelPath> destinationTravelPaths = RunDijkstra(endPointCities[1], true);

        ValueTuple<int, int> directPathKey = ValueTuple.Create(endPointCities[0], endPointCities[1]);
        if (originTravelPaths.ContainsKey(directPathKey))
        {
            TravelPath directPathClone = new TravelPath(originTravelPaths[directPathKey].path, originTravelPaths[directPathKey].cost);
            allTravelpaths.Add(directPathClone);
        }

        foreach (int mergeCity in allCityIds)
        {
            ValueTuple<int, int> path1Key = ValueTuple.Create(endPointCities[0], mergeCity);
            ValueTuple<int, int> path2Key = ValueTuple.Create(endPointCities[1], mergeCity);
            if (!(mergeCity == endPointCities[0] || mergeCity == endPointCities[1]) && originTravelPaths.ContainsKey(path1Key) && destinationTravelPaths.ContainsKey(path2Key))
            {
                TravelPath path1 = originTravelPaths[path1Key];
                TravelPath path2 = destinationTravelPaths[ValueTuple.Create(endPointCities[1], mergeCity)];
                if (path1.cost<infinity && path2.cost<infinity)
                {
                    List<int> thisPath = new List<int>();
                    thisPath.AddRange(path1.path);
                    thisPath.AddRange(path2.path.AsEnumerable().Reverse());
                    int thisCost = path1.cost + path2.cost;
                    allTravelpaths.Add(new TravelPath(thisPath, thisCost));
                }
            }
        }
        return allTravelpaths;
    }

    private void printPath(int[] path, int start, int end)
    {
        System.Diagnostics.Debug.WriteLine("Shortest path from {0} to {1}: (cityIds)", start, end);
        int temp = end;
        Stack<int> s = new Stack<int>();
        while (temp != start)
        {
            s.Push(temp);
            temp = path[temp];
        }
        System.Diagnostics.Debug.Write(temp.ToString() + ", ");
        while (s.Count != 0)
        {
            System.Diagnostics.Debug.Write(s.Pop().ToString() + ", ");
        }
    }

    private List<int> getPath(int[] path, int start, int end, bool popLast = false)
    {
        int temp = end;
        Stack<int> s = new Stack<int>();
        while (temp != start)
        {
            s.Push(temp);
            temp = path[temp];
        }
        if (popLast && s.Count>0)
        {
            s.Pop();
        }
        s.Push(start);

        return s.ToList();
    }

    public void AddEdge(int vertexA, int vertexB, int distance)
    {
        if (vertexA > 0 && vertexB > 0 && vertexA <= graphSize && vertexB <= graphSize)
        {
            adjMatrix[vertexA, vertexB] = distance;
        }
    }
    public void RemoveEdge(int vertexA, int vertexB)
    {
        if (vertexA > 0 && vertexB > 0 && vertexA <= graphSize && vertexB <= graphSize)
        {
            adjMatrix[vertexA, vertexB] = 0;
        }
    }
    public bool Adjacent(int vertexA, int vertexB)
    {
        return (adjMatrix[vertexA, vertexB] > 0);
    }
    public int length(int vertex_u, int vertex_v)
    {
        return adjMatrix[vertex_u, vertex_v];
    }
    public void Display()
    {
        System.Diagnostics.Debug.WriteLine("***********Adjacency Matrix Representation***********");
        System.Diagnostics.Debug.WriteLine("Number of nodes: {0}\n", graphSize - 1);
        foreach (NodeData n in vertices)
        {
            System.Diagnostics.Debug.Write("{0}\t", n.data);
        }
        System.Diagnostics.Debug.WriteLine("");
        for (int i = 1; i < graphSize; i++)
        {
            System.Diagnostics.Debug.Write("{0}\t", vertices[i].data);
            for (int j = 1; j < graphSize; j++)
            {
                System.Diagnostics.Debug.Write("{0}\t", adjMatrix[i, j].ToString());
            }
            System.Diagnostics.Debug.WriteLine("");
            System.Diagnostics.Debug.WriteLine("");
        }
        System.Diagnostics.Debug.WriteLine("Read the graph from left to right");
        System.Diagnostics.Debug.WriteLine("Example: Node A has an edge to Node C with distance: {0}",
            length(1, 3));
    }
    private void DisplayNodeData(int v)
    {
        System.Diagnostics.Debug.WriteLine(vertices[v].data);
    }
}

