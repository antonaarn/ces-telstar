﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Telnet.Web.Models
{
    public class ResultsModel
    {
        public bool Accept { get; set; }

        public double Price { get; set; }

        public int Time { get; set; }

        public long OriginID { get; set; }

        public long DestinationID { get; set; }

        public string Operators { get; set; }

        public DateTime CreateTime { get; set; }
    }
}
