﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

public class AlgorithmTest
{

    Graph gr;
    int graphSize;
    List<(string cityName, int cityId)> Cities;
    List<(int cityFromIndex, int cityToIndex, int cost)> travelcosts;
    List<int> endPointCities;

    public AlgorithmTest()
    {
;
        Cities = new List<(string cityName, int cityId)> { ("De kanariske oer", 0), ("Tanger", 1), ("Marrakesh", 2), ("Tunis", 3), ("Sahara", 4), ("Tripoli", 5) };
        travelcosts = new List<(int cityFromIndex, int cityToIndex, int cost)> { (0, 0, 0), (1, 1, 1), (1,2,2), (2,1,2), (1,3,5), (3,1,5), (1,4,5), (4,1,5), (2,2,0), (2,4,5), (4,2,5), (3,3,0), (3,5,3), (5,3,3), (4,4,0), (5,5,0) };
        endPointCities = new List<int> { 1, 3 };
        graphSize = 6;


        gr = new Graph(graphSize, Cities, travelcosts);
        List<TravelPath> travelResults = gr.computeMultiplePaths(endPointCities);
    }
}
