﻿using System.Collections.Generic;

/// <summary>
/// Stores the path traceled by djikstra and the cost accrued during this.
/// </summary>
public class TravelPath
{
    public List<int> path;
    public int cost;

    public TravelPath(List<int> path, int cost)
    {
        this.cost = cost;
        this.path = path;
    }
}

