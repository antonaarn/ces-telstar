﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Telnet.Lib;
using Telnet.Web.Data;
using Telnet.Web.Models;

namespace Telnet.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly ApplicationDbContext _dbContext;

        public HomeController(ILogger<HomeController> logger, ApplicationDbContext dbContext)
        {
            _logger = logger;
            _dbContext = dbContext;
        }

        public IActionResult Index()
        {
            //AlgorithmTest algTest = new AlgorithmTest();
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        public IActionResult Testapi()
        {
            //var response = ExternalRouteConnector.SendRouteRequest("https://localhost:44360/api/Route");

            //ViewData["Api"] = $"Response: Accept = {response.Accept}, Cost = {response.Cost}, Minutes = {response.Minutes}";
            return View();
        }

        public IActionResult TestRouteManager()
        {
            RouteManager routeManager = new RouteManager();
            //routeManager.PushDataToAnInternalRequest(new SearchModel());

            //var response = ExternalRouteConnector.SendRouteRequest("https://localhost:44360/api/Route");

            //ViewData["Api"] = $"Response: Accept = {response.Accept}, Cost = {response.Cost}, Minutes = {response.Minutes}";
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
