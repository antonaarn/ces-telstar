﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.VisualBasic.CompilerServices;
using Telnet.Web.Data;
using Telnet.Web.Models;
using Microsoft.Extensions.Logging;
using Telnet.Lib;

namespace Telnet.Web.Controllers
{

    public class TelstarController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly ApplicationDbContext _dbContext;

        public TelstarController(ILogger<HomeController> logger, ApplicationDbContext dbContext)
        {
            _logger = logger;
            _dbContext = dbContext;
        }

        public IActionResult Index()
        {

            return View();
        }
        /// <summary>
        /// Search to http post formula from UI. 
        /// </summary>
        /// <param name="search"></param>
        /// <returns>View ()</returns>
        [HttpPost]
        public ActionResult Search(SearchModel search)
        {
            string origin = search.Origin.Split(" (")[0];
            string destination = search.Destination.Split(" (")[0];
            double weight = search.Weight;
            double height = search.Height;
            double width = search.Width;
            double depth = search.Depth;
            bool recommended = search.Recommended;
            bool livestock = search.Livestock;
            bool refrigerated = search.Refrigerated;
            bool cautious = search.Cautious;
            search.Recommended = true;
          


            RouteManager routeManager = new RouteManager();
            InternalRouteSummary route = routeManager.PushDataToAnInternalRequest(search);

            var optionsBuilder = new DbContextOptionsBuilder<ApplicationDbContext>();
            IConfigurationRoot dupa = new ConfigurationBuilder()
            .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
            .AddJsonFile("appsettings.json")
            .Build();
            optionsBuilder.UseSqlServer(dupa.GetConnectionString("DefaultConnection"));
            ApplicationDbContext ctx = new ApplicationDbContext(optionsBuilder.Options);

            ViewBag.Cities = ctx.Cities.ToList().OrderBy(city => city.PartOfContinent).Select(city => city.DisplayName);

            if (!ModelState.IsValid)
            {
                return View();
            }

            //return (RedirectToAction("Results", new { 
            //    origin = origin,
            //    destination = destination,
            //    weight = weight,
            //    height = height,
            //    width = width,
            //    depth = depth,
            //    recommended = recommended,
            //    livestock = livestock,
            //    refrigerated = refrigerated,
            //    cautious = cautious
            //}));


            return (RedirectToAction("Results", route));

            //InternalRouteSummary[] fastestRoutes = new InternalRouteSummary[] { route };

            //return (RedirectToAction("Results", fastestRoutes[1]));

        }

        /// <summary>
        /// Entry point to the Search UI
        /// </summary>
        /// <returns>View</returns>
        public IActionResult Search()
        {
            
            var optionsBuilder = new DbContextOptionsBuilder<ApplicationDbContext>();
            IConfigurationRoot dupa = new ConfigurationBuilder()
            .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
            .AddJsonFile("appsettings.json")
            .Build();
            optionsBuilder.UseSqlServer(dupa.GetConnectionString("DefaultConnection"));
            ApplicationDbContext ctx = new ApplicationDbContext(optionsBuilder.Options);


            ViewBag.Cities = ctx.Cities.ToList().OrderBy(city => city.PartOfContinent).Select(city => city.DisplayName);


            return View();
        }

        public IActionResult Results(InternalRouteSummary eachOption)
        {

            ViewBag.eachOption = eachOption;

            ViewBag.fromShortcode = getCityShortcodeFromID((int) eachOption.OriginID);
            ViewBag.toShortcode = getCityShortcodeFromID((int) eachOption.DestinationID);

            return View();
        }

        public string getCityShortcodeFromID(int ID)
        {
            return Enum.GetName(typeof(Constants.CityShortcode), ID);
        }

        //public IActionResult Results(InternalRouteSummary[] AllOptions)
        //{

        //    ViewBag.AllOptions = AllOptions;

        //    return View();
        //}


    }
}
