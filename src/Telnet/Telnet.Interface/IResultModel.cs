﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Telnet.Interface
{
    public interface IResultModel
    {
        public bool Accept { get; set; }

        public double Price { get; set; }

        public int Time { get; set; }

        public long OriginID { get; set; }

        public long DestinationID { get; set; }

        public string Operators { get; set; }

        public DateTime CreateTime { get; set; }
    }
}
