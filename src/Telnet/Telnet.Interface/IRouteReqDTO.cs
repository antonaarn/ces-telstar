﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Telnet.Interface
{
    public interface IRouteReqDTO
    {
        public string Origin { get; set; }

        public string Destination { get; set; }

        public double Weight { get; set; }

        public double[] Size { get; set; }

        public string[] Categories { get; set; }

        public string Date { get; set; }
        
        public string Password { get; set; }
    }
}
