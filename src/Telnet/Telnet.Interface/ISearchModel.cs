﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Telnet.Interface
{
    public interface ISearchModel
    {
        public string Origin { get; set; }
        public string Destination { get; set; }
        public double Weight { get; set; }
        public double Height { get; set; }
        public double Width { get; set; }
        public double Depth { get; set; }
        public bool Recommended { get; set; }
        public bool Cautious { get; set; }
        public bool Livestock { get; set; }
        public bool Refrigerated { get; set; }
    }
}
