﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Telnet.Interface
{
    public interface IRouteRespDTO
    {
        public bool Accept { get; set; }

        public double Cost { get; set; }

        public double Minutes { get; set; }
    }
}
