﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Telnet.Lib
{
    /// <summary>
    /// Potential Route is used for sending information to UI
    /// </summary>
    public class PotentialRoute
    {

        public long OriginID { get; set; }

        public long DestinationID { get; set; }

        public DateTime CreateTime { get; set; }

        public double Weight { get; set; }

        public double[] Size { get; set; }

        public string[] Categories { get; set; }


    }
}
