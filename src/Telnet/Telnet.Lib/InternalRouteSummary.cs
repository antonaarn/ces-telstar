﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Telnet.Lib
{
    /// <summary>
    /// Internal Route Summary stores only Routes that can be operated by Telstar Logistics
    /// </summary>
    public class InternalRouteSummary : ExternalRouteSummary
    {
        public long OriginID { get; set; }

        public long DestinationID { get; set; }

        public string Operators { get; set; }

        public DateTime CreateTime { get; set; }

    }
}
