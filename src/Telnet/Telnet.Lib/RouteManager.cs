﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Telnet.Interface;

namespace Telnet.Lib
{
    /// <summary>
    /// RouteManager is a main point to integrate and starting point for calculation chepeast and fastest Route.
    /// </summary>
    public class RouteManager
    {
        public RouteManager()
        {
        }

        public void GetCheapestXRoutes(int numberOfRoutes, int firstCity, int secondCity)
        {

        }

        public void GetFastestXRoutes(int numberOfRoutes, int firstCity, int secondCity)
        {

        }


        public ExternalRouteSummary PushDataToAnExternalRequest(PotentialRoute potentialRoute)
        {
            //Call algorithm
            return new ExternalRouteSummary() { Accept = false, Price = 0, Time = 0 };
        }

        public InternalRouteSummary PushDataToAnInternalRequest(ISearchModel searchModel)
        {
            PotentialRoute potentialRoute = ConvertSearchModelToPotentialRoute(searchModel);

            if (potentialRoute == null)
                return new InternalRouteSummary() { Accept = false, Price = 0, Time = 0 };

            //Call external api and get all their nodes
            //send to graph

            //Call algorithm
            return new InternalRouteSummary()
            {
                Accept = false,
                Price = 0,
                Time = 0,
                OriginID = potentialRoute.OriginID,
                DestinationID = potentialRoute.DestinationID,
                CreateTime = potentialRoute.CreateTime
            };
        }

        public PotentialRoute ConvertSearchModelToPotentialRoute(ISearchModel searchModel)
        {
            PotentialRoute potentialRoute = new PotentialRoute();

            if (Enum.TryParse(typeof(Constants.CityShortcode), searchModel.Origin, out object originEnum))
            {
                potentialRoute.OriginID = (int)originEnum;
            }

            if (Enum.TryParse(typeof(Constants.CityShortcode), searchModel.Destination, out object destEnum))
            {
                potentialRoute.OriginID = (int)destEnum;
            }

            potentialRoute.CreateTime = DateTime.Now.Date;

            potentialRoute.Weight = searchModel.Weight;

            potentialRoute.Size = new double[] { searchModel.Width, searchModel.Depth, searchModel.Height };

            List<string> categories = new List<string>();

            if (searchModel.Livestock)
                categories.Add(Constants.Categories.LIV.ToString());

            if (searchModel.Cautious)
                categories.Add(Constants.Categories.CAU.ToString());

            if (searchModel.Recommended)
                categories.Add(Constants.Categories.REC.ToString());

            if (searchModel.Refrigerated)
                categories.Add(Constants.Categories.REF.ToString());

            potentialRoute.Categories = categories.ToArray();

            return potentialRoute;
        }

        public PotentialRoute ConvertRouteReqDTOToPotentialRoute(IRouteReqDTO routeReqDTO)
        {
            PotentialRoute potentialRoute = new PotentialRoute();

            if (Enum.TryParse(typeof(Constants.CityShortcode), routeReqDTO.Origin, out object originEnum))
            {
                potentialRoute.OriginID = (int)originEnum;
            }

            if (Enum.TryParse(typeof(Constants.CityShortcode), routeReqDTO.Destination, out object destEnum))
            {
                potentialRoute.OriginID = (int)destEnum;
            }

            potentialRoute.CreateTime = DateTime.Now.Date;

            potentialRoute.Weight = routeReqDTO.Weight;

            if(routeReqDTO.Size.Length == 3)
                potentialRoute.Size = new double[] { routeReqDTO.Size[0], routeReqDTO.Size[1], routeReqDTO.Size[2] };

            potentialRoute.Categories = routeReqDTO.Categories;

            return potentialRoute;
        }

        public void ConvertExternalSummaryToRouteRespDTO(ExternalRouteSummary externalSummary, ref IRouteRespDTO routeRespDTO)
        {
            routeRespDTO.Accept = externalSummary.Accept;
            routeRespDTO.Cost = externalSummary.Price;
            routeRespDTO.Minutes = externalSummary.Time;
        }

        public void ConvertInternalSummaryToResultModel(InternalRouteSummary internalSummary, ref IResultModel resultModel)
        {
            resultModel.Accept = internalSummary.Accept;
            resultModel.Price = internalSummary.Price;
            resultModel.Time = internalSummary.Time;
        }
    }
}
