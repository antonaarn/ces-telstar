﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Telnet.Lib
{
    /// <summary>
    /// External Summary stores only routes that can be operate either Oceanic or East India
    /// </summary>
    public class ExternalRouteSummary
    {
        public bool Accept { get; set; }

        public double Price { get; set; }

        public int Time { get; set; }
    }
}
