﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Telnet.Lib
{
    public static class Constants
    {
        public enum CityShortcode
        {
            ADD = 1,
            AMA,
            BAH,
            CAI,
            CON,
            DAK,
            DAR,
            DEK,
            DRA,
            GUL,
            HVA,
            KAB,
            KAG,
            KAM,
            KAS,
            LUA,
            MAR,
            OMD,
            SAH,
            SIE,
            SLA,
            STH,
            SUA,
            TAN,
            TIM,
            TRI,
            TUN,
            VIF,
            VIS,
            WAD,
            ZAN
        }

        public enum Categories
        {
            CAU,
            FRA,
            LIV,
            OTH,
            REC,
            REF,
            SEX,
            WEA
        }
    }
}
